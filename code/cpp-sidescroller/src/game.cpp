//
// Created by tobias on 06.03.21.
//

#include <SDL2/SDL_image.h>
#include "game.h"
#include "entities/missile.h"
#include "scoreboard/scoreboard.h"
#include "entities/enemies/chasingEnemy.h"
#include <iostream>

namespace spaceBattle {
    game::game(float width, float height, const char *&title, fs::path assetRoot)
        : m_assetRoot(assetRoot)
    {
        //Initialize SDL
        if(SDL_Init(SDL_INIT_VIDEO) < 0)
        {
            printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        }
        else
        {
            //Set texture filtering to linear
            if(!SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1") )
            {
                printf("Warning: Linear texture filtering not enabled!");
            }

            //Create window
            m_window = SDL_CreateWindow(
                    title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                    width, height, SDL_WINDOW_SHOWN );
            if(m_window == NULL)
            {
                printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
            }
            else
            {
                //Create renderer for window
                m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
                if(m_renderer == NULL)
                {
                    printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
                }
                else
                {
                    //Initialize renderer color
                    SDL_SetRenderDrawColor(m_renderer, 0xFF, 0xFF, 0xFF, 0xFF);

                    //Initialize PNG loading
                    int imgFlags = IMG_INIT_PNG;
                    if(!(IMG_Init( imgFlags ) & imgFlags))
                    {
                        printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    }
                }
            }
        }
    }

    game::~game() {
        //Free ship instance and associated img
        m_shipText->free();
        delete m_shipText;
        delete m_spaceShip;
        m_shipMissileText->free();
        delete m_shipMissileText;

        //Free background texture
        m_bgText->free();
        delete m_bgText;

        //Free enemy instance and associated texture
        m_basicEnemyText->free();
        delete m_basicEnemyText;
        m_enemyMissileText->free();
        delete m_enemyMissileText;

        // Delete icons
        delete m_missedEnemyIcon;
        delete m_missedEnemyIconEmpty;
        delete m_shipLivesIconEmpty;
        delete m_shipLivesIcon;

        shipDestroyed(0);

        delete m_destructAnim;

        //Destroy window
        SDL_DestroyRenderer(m_renderer);
        SDL_DestroyWindow(m_window);
        m_window = NULL;
        m_renderer = NULL;

        //Quit SDL subsystems
        IMG_Quit();
        SDL_Quit();
    }

    void game::run() {
        //Load media
        if(!loadMedia()) { printf("Failed to load media!\n"); }
        else
        {
            // Get window size
            int w = 0;
            int h = 0;
            SDL_GetWindowSize(m_window, &w, &h);

            // Create ship
            m_spaceShip = new ship(this, m_shipText, m_entityId++, m_destructAnim,
                                   glm::vec2(100, h / 2),
                                   glm::vec2(85, 85));


            bool displayStats = true;       // Show stats after game over
            m_spawnTimerBasic.start();      // Start spawn timer for basic enemies
            m_spawnTimerChasing.start();    // Start spawn timer for chasing enemies
            SDL_Event e;                    // Event handler

            //While application is running
            while(!m_quit)
            {
                //Handle events on queue
                while(SDL_PollEvent(&e) != 0)
                {
                    //User requests quit
                    if(e.type == SDL_QUIT)
                    {
                        m_quit = true;
                        displayStats = false;
                    }
                    handleEvents(e);
                }

                //Logic updates of game and entities
                update();

                //Physics updates - in this case only simple collision detection
                physics();

                //Render objects
                draw();
            }

            // Print current game stats and load scoreboard
            if (displayStats)
            {
                showStats();
            }
        }
    }

    bool game::loadMedia() {
        //Loading success flag
        bool success = true;

        //Load ship texture
        m_shipText = new texture();
        if(!m_shipText->loadFromFile(this, resolve("SpaceShip85x85.png")))
        {
            printf( "Failed to load ship texture!\n" );
            success = false;
        }

        //Load background texture
        m_bgText = new texture();
        if (!m_bgText->loadFromFile(this, resolve("MoonLandscape1600x900.png")))
        {
            printf( "Failed to load background texture!\n" );
            success = false;
        }

        // Load basic enemy texture
        m_basicEnemyText = new texture();
        if (!m_basicEnemyText->loadFromFile(this, resolve("BasicEnemy120x64.png")))
        {
            printf( "Failed to load basic enemy texture!\n" );
            success = false;
        }

        // Load chasing enemy texture
        m_chasingEnemyText = new texture();
        if (!m_chasingEnemyText->loadFromFile(this, resolve("ChasingEnemy123x96.png")))
        {
            printf( "Failed to load chasing enemy texture!\n" );
            success = false;
        }

        // Load ship missile texture
        m_shipMissileText = new texture();
        if (!m_shipMissileText->loadFromFile(this, resolve("MissileFriendly40x20.png")))
        {
            printf( "Failed to load ship missile texture!\n" );
            success = false;
        }

        // Load ship missile texture
        m_enemyMissileText = new texture();
        if (!m_enemyMissileText->loadFromFile(this, resolve("MissileHostile40x20.png")))
        {
            printf( "Failed to load enemy missile texture!\n" );
            success = false;
        }

        // Load missed enemies icon
        m_missedEnemyIcon = new texture();
        if (!m_missedEnemyIcon->loadFromFile(this, resolve("MissedEnemies32x32.png")))
        {
            printf( "Failed to load missed enemies texture!\n" );
            success = false;
        }
        m_missedEnemyIconEmpty = new texture();
        if (!m_missedEnemyIconEmpty->loadFromFile(this, resolve("MissedEnemiesEmpty32x32.png")))
        {
            printf( "Failed to load missed enemies empty texture!\n" );
            success = false;
        }

        // Load remaining lives icon
        m_shipLivesIcon = new texture();
        if (!m_shipLivesIcon->loadFromFile(this, resolve("Heart64x64.png")))
        {
            printf( "Failed to load heart icon!\n" );
            success = false;
        }
        m_shipLivesIconEmpty = new texture();
        if (!m_shipLivesIconEmpty->loadFromFile(this, resolve("HeartEmpty64x64.png")))
        {
            printf( "Failed to load heart empty icon!\n" );
            success = false;
        }

        // Load destruction animation for ship
        auto animText = new texture();
        if (!animText->loadFromFile(this, resolve("Explosion768x96.png")))
        {
            printf( "Failed to load explosion texture!\n" );
            success = false;
        }
        m_destructAnim = new animation(animText, 768, 96, 8, 15, false);

        return success;
    }

    void game::handleEvents(SDL_Event &e) {
        m_spaceShip->handleEvents(this, e);
        for(auto &kvp : m_enemies){ kvp.second->handleEvents(this, e); }
        for(auto &kvp : m_shipMissiles){ kvp.second->handleEvents(this, e); }
        for(auto &kvp : m_enemyMissiles){ kvp.second->handleEvents(this, e); }
    }

    void game::update() {
        //Calculate time step
        float timeStep = m_stepTimer.getTicks() / 1000.f;

        // call update on entities
        m_spaceShip->update(this, timeStep);
        for(auto &kvp : m_enemies) { kvp.second->update(this, timeStep); }
        for(auto &kvp : m_shipMissiles) { kvp.second->update(this, timeStep); }
        for(auto &kvp : m_enemyMissiles) { kvp.second->update(this, timeStep); }

        // scroll bg img
        m_scrollingOffset -= m_scrollingSpeed * timeStep;
        if(m_scrollingOffset < -m_bgText->getWidth()) { m_scrollingOffset = 0; }

        // delete entities that have been marked for deletion
        deleteEntities();

        // Spawn logic for enemies
        spawnEnemies();

        //Restart step timer
        m_stepTimer.start();
    }

    void game::physics() {
        // check collisions on entities
        m_spaceShip->physics(this, nullptr, &m_enemyMissiles);
        for(auto &kvp : m_enemies) { kvp.second->physics(this, m_spaceShip, &m_shipMissiles); }
        for(auto &kvp : m_shipMissiles) { kvp.second->physics(); }
        for(auto &kvp : m_enemyMissiles) { kvp.second->physics(); }
    }

    void game::draw() {
        //Clear screen
        SDL_SetRenderDrawColor(m_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(m_renderer);

        // draw scrolling bg
        m_bgText->render(this, m_scrollingOffset, 0);
        m_bgText->render(this, m_scrollingOffset + m_bgText->getWidth(), 0);

        // call draw on entities
        for(auto &kvp : m_enemyMissiles) { kvp.second->draw(this); }
        for(auto &kvp : m_shipMissiles) { kvp.second->draw(this); }
        for(auto &kvp : m_enemies) { kvp.second->draw(this); }
        m_spaceShip->draw(this);

        // Draw lives and missed enemies icons
        int gapX = 10;
        int gapY = 10;
        int posX = gapX;
        // lives
        for (int i = 0; i < m_spaceShip->getLives(); ++i) {
            m_shipLivesIcon->render(this, posX, gapY);
            posX += m_shipLivesIcon->getWidth() + gapX;
        }
        for (int i = 0; i < 3 - m_spaceShip->getLives(); ++i) {
            m_shipLivesIconEmpty->render(this, posX, gapY);
            posX += m_shipLivesIconEmpty->getWidth() + gapX;
        }
        posX += 20;
        // missed enemies
        for (int i = 0; i < m_spaceShip->getMissedEnemiesCount(); ++i) {
            m_missedEnemyIcon->render(this, posX, gapY);
            posX += m_missedEnemyIcon->getWidth() + gapX;
        }
        for (int i = 0; i < m_spaceShip->getMaxMissedEnemies() - m_spaceShip->getMissedEnemiesCount(); ++i) {
            m_missedEnemyIconEmpty->render(this, posX, gapY);
            posX += m_missedEnemyIconEmpty->getWidth() + gapX;
        }

        //Update screen
        SDL_RenderPresent(m_renderer);
    }

    std::string game::resolve(const fs::path &relativeAssetPath) const {
        fs::path path = m_assetRoot / relativeAssetPath;
        std::string resolvedPath = "";
        try {
            resolvedPath = fs::canonical(path.make_preferred());
        } catch (const std::exception &e) {
            printf("Error: %s\n", e.what());
            printf("Failed to resolve path %s\n", path.c_str());
        }
        return resolvedPath;
    }

    void game::deleteEnemy(int id) { m_deletedEnemies.push_back(id); }

    void game::spawnMissile(missileAffiliation affiliation, glm::vec2 spawnPos, glm::vec2 velocity) {
        if (affiliation == friendly) {
            auto m = new missile(this, m_entityId, m_shipMissileText, spawnPos, friendly, velocity);
            m_shipMissiles[m_entityId] = m;
            m_entityId++;
        }
        if (affiliation == hostile) {
            auto m = new missile(this, m_entityId, m_enemyMissileText, spawnPos, hostile,
                                 glm::vec2(-velocity.x, velocity.y));
            m_enemyMissiles[m_entityId] = m;
            m_entityId++;
        }
    }

    void game::deleteMissile(missileAffiliation affiliation, int id) {
        if (affiliation == friendly) { m_deletedShipMissiles.push_back(id); }
        else if (affiliation == hostile) { m_deletedEnemyMissiles.push_back(id); }
    }

    void game::deleteEntities() {
        // Delete enemies
        for(auto id : m_deletedEnemies)
        {
            entity* enemy = m_enemies[id];
            m_enemies.erase(id);
            delete enemy;
        }
        m_deletedEnemies.clear();

        // Delete ship missiles
        for(auto id : m_deletedShipMissiles)
        {
            entity* m = m_shipMissiles[id];
            m_shipMissiles.erase(id);
            delete m;
        }
        m_deletedShipMissiles.clear();

        // Delete enemy missiles
        for(auto id : m_deletedEnemyMissiles)
        {
            entity* m = m_enemyMissiles[id];
            m_enemyMissiles.erase(id);
            delete m;
        }
        m_deletedEnemyMissiles.clear();
    }

    void game::spawnEnemies() {
        // Spawn basic enemies -> Increase spawn amount per 10 sec with time passing
        m_spawnIntervalBasic = 10000 / m_basicEnemiesPer10sec;
        if (m_spawnTimerBasic.getTicks() > m_spawnIntervalBasic)
        {
            // Get window size
            int w = 0;
            int h = 0;
            SDL_GetWindowSize(m_window, &w, &h);

            // Get random height
            float y = (h * (std::rand() % 95)) / 100;

            // Spawn basic enemy
            m_enemies[m_entityId] = new basicEnemy(
                    this, m_entityId, m_basicEnemyText, m_spaceShip,
                    glm::vec2(w, y));
            m_entityId++;

            // reset spawn timer
            m_spawnTimerBasic.start();
        }
        // every 10 sec + 0.2
        m_basicEnemiesPer10sec = 2 + SDL_GetTicks() / 40000;


        // Spawn chasing enemies -> Increase spawn amount per 10 sec with time passing
        m_spawnIntervalChasing = 10000 / m_basicEnemiesPer10sec;
        if (m_spawnTimerChasing.getTicks() > m_spawnIntervalChasing)
        {
            // Get window size
            int w = 0;
            int h = 0;
            SDL_GetWindowSize(m_window, &w, &h);

            // Get random height
            float y = (h * (std::rand() % 95)) / 100;

            // Spawn chasingEnemy
            m_enemies[m_entityId] = new chasingEnemy(
                    this, m_entityId, m_chasingEnemyText, m_spaceShip,
                    glm::vec2(w, y));
            m_entityId++;

            // reset spawn timer
            m_spawnTimerChasing.start();
        }
        // every 10 sec + 0.1
        m_chasingEnemiesPer10sec = 1.5 + SDL_GetTicks() / 100000;
    }

    void game::showStats() {
        scoreboard sb;
        sb.loadData(this);

        std::string input;
        time_t curTime = time(0);
        std::string cDate = ctime(&curTime);
        std::string date = cDate.substr(0, cDate.size() - 1);

        std::cout << "Enter your name:" << std::endl;
        std::cin >> input;
        std::cout << "\nGAME STATS:" <<
                  "\nName: " << input <<
                  "\nScore: " << m_spaceShip->getPoints() <<
                  "\nDate and time: " << date << "\n" << std::endl;

        scoreboardEntry newEntry = { m_spaceShip->getPoints(), input, date };
        sb.addEntry(newEntry);

        std::cout << "Show scoreboard? [y/n]" << std::endl;
        std::cin >> input;
        if (input == "y") {
            printf("SCOREBOARD:\n");
            sb.print();
        }

        sb.saveData(this);
    }

    void game::shipDestroyed(int remainingLives) {
        // Delete enemies
        for(auto e : m_enemies) { m_deletedEnemies.push_back(e.first); }

        // Delete ship missiles
        for(auto e : m_shipMissiles) { m_deletedShipMissiles.push_back(e.first); }

        // Delete enemy missiles
        for(auto e : m_enemyMissiles) { m_deletedEnemyMissiles.push_back(e.first); }

        if (remainingLives <= 0) m_quit = true;
    }
}