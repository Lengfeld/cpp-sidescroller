//
// Created by tobias on 14.03.21.
//

#pragma once

#include <string>
#include "texture.h"

namespace spaceBattle {
    class game;

    class animation {
    public:
        // owns and manages texture ptr
        animation(texture* texture, int textWidth, int textHeight, int totalAnimFrames, float animSpeed = 15, bool loop = false);
        ~animation();

        void free(); // deallocates memory of sprite used
        void reset(); // if not looped use reset to play again
        bool render(game* game, float deltaTime, int x, int y); // plays & renders animation, returns true while still playing

    private:
        SDL_Rect* createSpriteClips() const;

        texture*        m_texture;

        const float     m_animSpeed = 15;
        float           m_timePassed = 0;
        int             m_curAnimFrame = 0;
        const int       m_totalAnimFrames = 8;

        SDL_Rect*       m_spriteClips;

        const bool      m_loop = false;
        bool            m_playAnim = true;

        const int       m_textHeight;
        const int       m_textWidth;
    };
}


