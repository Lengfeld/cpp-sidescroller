//
// Created by tobias on 14.03.21.
//

#include "animation.h"
#include "../game.h"

namespace spaceBattle {
    animation::animation(texture* texture, int textWidth, int textHeight, int totalAnimFrames,
                         float animSpeed, bool loop) :
            m_texture(texture),
            m_totalAnimFrames(totalAnimFrames),
            m_loop(loop),
            m_animSpeed(animSpeed),
            m_textWidth(textWidth),
            m_textHeight(textHeight)
    {
        m_spriteClips = createSpriteClips();
    }

    animation::~animation() {
        free();
    }

    void animation::free() {
        //Free texture
        delete m_texture;
        delete[] m_spriteClips;
    }

    bool animation::render(game *game, float deltaTime, int x, int y) {
        bool curPlaying = false;

        if (m_playAnim) {
            //Get window size
            int width = 0;
            int height = 0;
            SDL_GetWindowSize(game->getWindow(), &width, &height);

            //Render current frame
            SDL_Rect* currentClip = &m_spriteClips[m_curAnimFrame];
            m_texture->render(game, x, y, currentClip);
            //Go to next frame
            m_timePassed += m_animSpeed * deltaTime;
            m_curAnimFrame = floor(m_timePassed);

            //Cycle animation
            if(m_curAnimFrame >= m_totalAnimFrames)
            {
                m_curAnimFrame = 0;
                m_timePassed = 0;
                if (!m_loop) { m_playAnim = false; }
            }
            curPlaying = true;
        }

        return curPlaying;
    }

    SDL_Rect * animation::createSpriteClips() const {

        SDL_Rect* clips = new SDL_Rect[m_totalAnimFrames];
        int clipWidth = m_textWidth / m_totalAnimFrames;

        for (int i = 0; i < m_totalAnimFrames; ++i) {
            //Set sprite clips
            clips[i].x = clipWidth * i;
            clips[i].y = 0;
            clips[i].w = clipWidth;
            clips[i].h = m_textHeight;
        }

        return clips;
    }

    void animation::reset() {
        m_timePassed = 0;
        m_curAnimFrame = 0;
        m_playAnim = true;
    }
}