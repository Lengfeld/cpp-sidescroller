//
// Created by tobias on 07.03.21.
//

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>

namespace spaceBattle {
    class game;

    class texture {
    public:
        //Initializes variables
        texture();

        //Deallocates memory
        ~texture();

        //Loads image at specified path
        bool loadFromFile(game *game, std::string path);

        //Deallocates texture
        void free();

        //Set color modulation
        void setColor(Uint8 red, Uint8 green, Uint8 blue);

        //Renders texture at given point
        void render(game *game, int x, int y,
                    SDL_Rect* clip = nullptr, double angle = 0.0,
                    SDL_Point* center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE);

        //Gets image dimensions
        int getWidth() const;
        int getHeight() const;

    private:
        //The actual hardware texture
        SDL_Texture*    m_texture = NULL;

        //Image dimensions
        int             m_width;
        int             m_height;
    };
}
