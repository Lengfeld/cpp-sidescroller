//
// Created by tobias on 07.03.21.
//

#include "texture.h"
#include "../game.h"

namespace spaceBattle {

    texture::texture() {
        //Initialize
        m_texture = NULL;
        m_width = 0;
        m_height = 0;
    }

    texture::~texture()
    {
        //Deallocate
        free();
    }

    bool texture::loadFromFile(game *game, std::string path)
    {
        //Get rid of preexisting texture
        free();

        //The final texture
        SDL_Texture* newTexture = NULL;

        //Load image at specified path
        SDL_Surface* loadedSurface = IMG_Load(path.c_str());
        if(loadedSurface == NULL)
        {
            printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
        }
        else
        {
            //Color key image
            SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

            //Create texture from surface pixels
            newTexture = SDL_CreateTextureFromSurface(game->getRenderer(), loadedSurface);
            if(newTexture == NULL)
            {
                printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
            }
            else
            {
                //Get image dimensions
                m_width = loadedSurface->w;
                m_height = loadedSurface->h;
            }

            //Get rid of old loaded surface
            SDL_FreeSurface(loadedSurface);
        }

        //Return success
        m_texture = newTexture;
        return m_texture != NULL;
    }

    void texture::free()
    {
        //Free texture if it exists
        if(m_texture != nullptr)
        {
            SDL_DestroyTexture(m_texture);
            m_texture = NULL;
            m_width = 0;
            m_height = 0;
        }
    }

    void texture::setColor(Uint8 red, Uint8 green, Uint8 blue)
    {
        //Modulate texture rgb
        SDL_SetTextureColorMod(m_texture, red, green, blue);
    }

    void texture::render(game *game, int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
    {
        //Set rendering space and render to screen
        SDL_Rect renderQuad = { x, y, m_width, m_height };

        //Set clip rendering dimensions
        if(clip != nullptr)
        {
            renderQuad.w = clip->w;
            renderQuad.h = clip->h;
        }

        //Render to screen
        SDL_RenderCopyEx(game->getRenderer(), m_texture, clip, &renderQuad, angle, center, flip);
    }

    int texture::getWidth() const { return m_width; }

    int texture::getHeight() const { return m_height; }
}