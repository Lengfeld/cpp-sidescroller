//
// Created by tobias on 11.03.21.
//

#pragma once

#include <list>
#include "../game.h"

namespace spaceBattle {
    struct scoreboardEntry {
        int         score;
        std::string name;
        std::string date;
    };

    class scoreboard {
    public:
        scoreboard();
        ~scoreboard();

        bool loadData(game* game);
        bool saveData(game* game);
        void addEntry(scoreboardEntry& entry);
        void print();

    private:
        const int                   m_maxEntries = 10;
        // entry with biggest score at end
        std::list<scoreboardEntry>  m_scoreboardEntries;
    };
}

