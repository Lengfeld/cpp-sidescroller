//
// Created by tobias on 11.03.21.
//

#include <iostream>
#include <fstream>
#include <experimental/filesystem>
#include "scoreboard.h"

namespace spaceBattle {
    scoreboard::scoreboard() { }

    scoreboard::~scoreboard() { }

    bool scoreboard::loadData(game* game) {

        std::ofstream outFile;
        fs::path path = game->resolve("score");

        // if file does not exist
        if (path.empty())
        {
            // Create new file 'score'
            path = game->getAssetRoot() / fs::path("score");
            outFile.open(path);

            if (outFile.fail()) {
                printf("Error while creating new save file.");
                outFile.close();
                return false;
            }

            // Fill with empty entries
            for(int i = 0; i < m_maxEntries; ++i)
            {
                scoreboardEntry newEntry = { 0, "-", "-" };

                int nameLength = newEntry.name.size() + 1;
                int dateLength = newEntry.date.size() + 1;

                outFile
                << newEntry.score << std::endl
                << nameLength
                << newEntry.name << std::endl
                << dateLength
                << newEntry.date << std::endl;

                m_scoreboardEntries.push_back(newEntry);
            }

            outFile.close();
        }
        // if file already exists
        else
        {
            std::ifstream inFile;
            inFile.open(path);

            if (outFile.fail())
            {
                printf("Error while opening saved file.");
                inFile.close();
                return false;
            }

            for(int i = 0; i < m_maxEntries; ++i)
            {
                scoreboardEntry entry;

                // Get score and name length
                long nameLength;
                inFile >> entry.score >> nameLength;

                // Get name
                char* name = new char[nameLength];
                inFile.getline(name, nameLength);

                // Get date
                long dateLength;
                inFile >> dateLength;

                char* date = new char[dateLength];
                inFile.getline(date, dateLength);

                // Add date and name to entry
                entry.name = std::string(name);
                entry.date = std::string(date);

                // add entry to scoreboard
                m_scoreboardEntries.push_back(entry);

                delete[] name;
                delete[] date;
            }

            inFile.close();
        }

        return true;
    }

    bool scoreboard::saveData(game* game) {

        std::ofstream outFile;
        fs::path path = game->resolve("score");

        // if file does not exist
        if (!path.empty())
        {
            outFile.open(path);

            if (outFile.fail()) {
                printf("Error while opening save file.");
                outFile.close();
                return false;
            }

            // Fill with empty entries
            for (auto &entry : m_scoreboardEntries)
            {
                long nameLength = entry.name.size() + 1;
                long dateLength = entry.date.size() + 1;

                outFile
                << entry.score << std::endl
                << nameLength
                << entry.name << std::endl
                << dateLength
                << entry.date << std::endl;
            }

            m_scoreboardEntries.clear();
            outFile.close();
        }
        else
        {
            printf("Error: save file not found. Could not save highscore.");
            return false;
        }

        return true;
    }

    void scoreboard::addEntry(scoreboardEntry& entry) {
        // list sorted by score, smallest at beginning
        std::list<scoreboardEntry>::iterator it;
        for (it = m_scoreboardEntries.begin(); it != m_scoreboardEntries.end(); ++it)
        {
            if (it->score >= entry.score)
            {
                break;
            }
        }

        // inserts new entry in front of the first entry with equal/bigger score
        // if new entry has the biggest score, it needs to be pushed after the previous biggest
        if (it->score < entry.score) { m_scoreboardEntries.push_back(entry); }
        else m_scoreboardEntries.insert(it, entry);

        // erase entry with smallest score to keep scoreboard at 10 entries
        m_scoreboardEntries.erase(m_scoreboardEntries.begin());
    }

    void scoreboard::print() {
        m_scoreboardEntries.reverse();

        int i = 1;
        for(auto &e : m_scoreboardEntries){
            std::cout << i++ << ".\t" << "Score: " << e.score << ",\tDate: " << e.date << ",\tName: " << e.name << std::endl;
        }

        m_scoreboardEntries.reverse();
    }
}