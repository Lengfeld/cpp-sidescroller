//Using SDL and standard IO
#include <SDL2/SDL.h>
#include <cstdio>
#include <experimental/filesystem>

#include "game.h"

#define GET_STRING(x) #x
#define GET_DIR(x) GET_STRING(x)

//Screen dimension constants
const int SCREEN_WIDTH = 1600;
const int SCREEN_HEIGHT = 900;
const char* SCREEN_TITLE = "Space Battle";

int main( int argc, char* args[] )
{
    // Get path to asset folder
    namespace fs = std::experimental::filesystem;
    auto assetPath = GET_DIR(DEBUG_ASSET_ROOT) /fs::path("assets");
    assetPath = fs::canonical(assetPath).make_preferred();

    // Try creating game
    try {
        spaceBattle::game spaceBattle(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE, assetPath);
        spaceBattle.run();
    } catch (const std::exception &e) {
        printf("Unhandled Exception: %s\n", e.what());
    }

    return 0;
}