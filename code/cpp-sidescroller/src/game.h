//
// Created by tobias on 06.03.21.
//
#pragma once

#include <experimental/filesystem>
#include <SDL2/SDL.h>
#include <map>
#include "entities/ship.h"
#include "entities/enemies/basicEnemy.h"
#include "timer/timer.h"
#include "rendering/animation.h"

namespace fs = std::experimental::filesystem;

namespace spaceBattle {
    class texture;

    enum missileAffiliation {
        friendly,
        hostile
    };

    class game {
    public:
        game(float width, float height, const char* &title, fs::path assetRoot = "./assets");
        ~game();
        void run();

        // returns empty string, when path not found
        std::string resolve(const fs::path &relativeAssetPath) const;
        fs::path getAssetRoot() const { return m_assetRoot; }
        SDL_Window *getWindow() const { return m_window; }
        SDL_Renderer *getRenderer() const { return m_renderer; }

        void deleteEnemy(int id);
        // velocity param: passed x needs to be positive
        void spawnMissile(missileAffiliation affiliation, glm::vec2 spawnPos, glm::vec2 velocity = glm::vec2(800, 0));
        void deleteMissile(missileAffiliation affiliation, int id);
        void shipDestroyed(int remainingLives);

    private:
        bool loadMedia();

        void update();
        void draw();
        void handleEvents(SDL_Event &e);
        void physics();

        void deleteEntities();
        void spawnEnemies();
        void showStats();

        SDL_Window*             m_window = NULL;
        SDL_Renderer*           m_renderer = NULL;
        fs::path                m_assetRoot;
        timer                   m_stepTimer;
        bool                    m_quit = false;

        float                   m_timeStep = 0.0f;

        // Scrolling background
        texture*                m_bgText = NULL;
        float                   m_scrollingOffset = 0;
        const float             m_scrollingSpeed = 375;

        // Entity tracking
        int                     m_entityId = 0;

        // Ship controlled by player
        texture*                m_shipText = NULL;
        ship*                   m_spaceShip = NULL;
        animation*              m_destructAnim;

        // Ship missiles
        texture*                m_shipMissileText = NULL;
        std::map<int, entity*>  m_shipMissiles;
        std::vector<int>        m_deletedShipMissiles;

        // Enemies
        std::map<int, entity*>  m_enemies;
        std::vector<int>        m_deletedEnemies;
        // basic enemy
        texture*                m_basicEnemyText = NULL;
        timer                   m_spawnTimerBasic;
        float                   m_spawnIntervalBasic = 0;
        float                   m_basicEnemiesPer10sec = 0;
        // chasing enemy
        texture*                m_chasingEnemyText = NULL;
        timer                   m_spawnTimerChasing;
        float                   m_spawnIntervalChasing = 0;
        float                   m_chasingEnemiesPer10sec = 0;

        // Enemy missiles
        texture*                m_enemyMissileText = NULL;
        std::map<int, entity*>  m_enemyMissiles;
        std::vector<int>        m_deletedEnemyMissiles;

        // Icons
        texture*                m_missedEnemyIcon;
        texture*                m_missedEnemyIconEmpty;
        texture*                m_shipLivesIcon;
        texture*                m_shipLivesIconEmpty;
    };
}


