//
// Created by tobias on 10.03.21.
//

#pragma once

#include "entity.h"
#include "ship.h"

namespace spaceBattle {
    class missile : public entity{
    public:
        explicit missile(game* game, int id, texture* texture,
                         glm::vec2 position,
                         missileAffiliation affiliation,
                         glm::vec2 velocity,
                         glm::vec2 size = glm::vec2(40.0f, 20.0f));

        void update(game *game, float deltaTime) override;

    private:
        missileAffiliation m_affiliation;
    };
}


