//
// Created by tobias on 10.03.21.
//

#include "missile.h"
#include "../game.h"

namespace spaceBattle {
    missile::missile(game *game, int id, texture *texture, glm::vec2 position,
                     missileAffiliation affiliation,
                     glm::vec2 velocity, glm::vec2 size) :
        entity(game, texture, id, position,  size,velocity),
        m_affiliation(affiliation)
    { }

    void missile::update(game *game, float deltaTime) {
        //Get window size
        int w = 0;
        int h = 0;
        SDL_GetWindowSize(game->getWindow(), &w, &h);

        //Move the missile left
        m_position.x += m_velocity.x * deltaTime;

        //If the missile went too far to the left or right
        if(m_position.x < 0 || m_position.x > w)
        {
            game->deleteMissile(m_affiliation, m_id);
        }
    }
}