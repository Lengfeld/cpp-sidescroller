//
// Created by tobias on 07.03.21.
//

#include "ship.h"
#include "../game.h"

namespace spaceBattle {
    ship::ship(game *game, texture* texture, int id, animation* destructAnim,
               glm::vec2 position, glm::vec2 size, float speed, int lives) :
            entity(game, texture, id, position, size),
            m_destructAnim(destructAnim),
            m_speed(speed),
            m_lives(lives)
    {
        m_missileTimer.start();
    }

    void ship::update(game* game, float deltaTime) {
        // destroy if too many enemies got past ship
        if (m_missedEnemies >= m_maxMissedEnemies) { destroy(game); }

        m_curTimeStep = deltaTime;

        //Get window size
        int w = 0;
        int h = 0;
        SDL_GetWindowSize(game->getWindow(), &w, &h);

        //Move the ship left or right
        m_position.x += m_velocity.x * deltaTime;

        //If the ship went too far to the left or right
        if((m_position.x < 0) || (m_position.x + m_size.x > w))
        {
            //Move back
            m_position.x -= m_velocity.x * deltaTime;
        }

        //Move the ship up or down
        m_position.y += m_velocity.y * deltaTime;

        //If the ship went too far up or down
        if((m_position.y < 0) || (m_position.y + m_size.y > h))
        {
            //Move back
            m_position.y -= m_velocity.y * deltaTime;
        }

        // Shoot missiles when timer reached interval
        if (m_shootMissiles && m_missileTimer.getTicks() > m_fireRate)
        {
            game->spawnMissile(
                    friendly,
                    glm::vec2(m_position.x + m_size.x, m_position.y + 0.2 * m_size.y));

            game->spawnMissile(
                    friendly,
                    glm::vec2(m_position.x + m_size.x, m_position.y + 0.8 * m_size.y));

            // restart timer
            m_missileTimer.start();
        }
    }

    void ship::handleEvents(game* game, SDL_Event &e) {
        //If a key was pressed
        if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
        {
            //Adjust the velocity
            switch(e.key.keysym.sym)
            {
                case SDLK_w: m_velocity.y -= m_speed; break;
                case SDLK_s: m_velocity.y += m_speed; break;
                case SDLK_a: m_velocity.x -= m_speed; break;
                case SDLK_d: m_velocity.x += m_speed; break;
                case SDLK_SPACE:
                    if(!m_destroyed) { m_shootMissiles = true; }
                    break;
            }
        }
        //If a key was released
        else if(e.type == SDL_KEYUP && e.key.repeat == 0)
        {
            //Adjust the velocity
            switch(e.key.keysym.sym)
            {
                case SDLK_w: m_velocity.y += m_speed; break;
                case SDLK_s: m_velocity.y -= m_speed; break;
                case SDLK_a: m_velocity.x += m_speed; break;
                case SDLK_d: m_velocity.x -= m_speed; break;
                case SDLK_SPACE:
                    m_shootMissiles = false;
                    break;
            }
        }
    }

    void ship::physics(game *game, ship *ship, std::map<int, entity *> *missiles) {
        if (missiles != nullptr)
        {
            for(auto &kvp : *missiles)
            {
                //The sides of the rectangles
                int leftShip, leftMissile;
                int rightShip, rightMissile;
                int topShip, topMissile;
                int bottomShip, bottomMissile;

                //Calculate the sides of the ship's rect
                leftShip = getPosition().x;
                rightShip = getPosition().x + getSize().x;
                topShip = getPosition().y;
                bottomShip = getPosition().y + getSize().y;

                //Calculate the sides of the missile's rect
                leftMissile = kvp.second->getPosition().x;
                rightMissile = kvp.second->getPosition().x + kvp.second->getSize().x;
                topMissile = kvp.second->getPosition().y;
                bottomMissile = kvp.second->getPosition().y + kvp.second->getSize().y;

                //If any of the sides from the ship are outside of the missile's rect
                if(bottomShip <= topMissile) continue;
                if(topShip >= bottomMissile) continue;
                if(rightShip <= leftMissile) continue;
                if(leftShip >= rightMissile) continue;

                //If none of the sides from the ship's rect are outside the missile's rect
                game->deleteMissile(hostile, kvp.second->getId());
                destroy(game);
            }
        }
    }

    void ship::draw(game *game) {
        if (!m_destroyed)
        {
            m_texture->render(game, m_position.x, m_position.y);
        }
        else
        {
            // sets m_destroyed to false as soon as destruction animation has played
            m_destroyed = m_destructAnim->render(game, m_curTimeStep, m_position.x, m_position.y);
            if (!m_destroyed) { m_destructAnim->reset(); }
        }
    }

    void ship::destroy(game* game)
    {
        m_lives = m_lives - 1;
        m_missedEnemies = 0;
        m_destroyed = true;
        printf("Ship destroyed! Lives: %d / 3.\n", m_lives);
        game->shipDestroyed(m_lives);
    }

    void ship::missedEnemy()
    {
        ++m_missedEnemies;
        printf("Missed enemies: %d / %d.\n", m_missedEnemies, m_maxMissedEnemies);
    }

    void ship::addPoints(int amount) { m_points += amount; }

    int ship::getPoints() const { return m_points; }
}