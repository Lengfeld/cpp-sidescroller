//
// Created by tobias on 07.03.21.
//

#include "entity.h"

namespace spaceBattle {
    entity::entity(class game *game, texture* texture, int id, glm::vec2 position, glm::vec2 size, glm::vec2 velocity) :
            m_texture(texture),
            m_id(id),
            m_position(position),
            m_velocity(velocity),
            m_size(size)
            { }

    void entity::update(game* game, float deltaTime) {
        // update logic
    }

    void entity::handleEvents(game* game, SDL_Event &e) {
        // handle events
    }

    void entity::draw(game *game) {
        m_texture->render(game, m_position.x, m_position.y);
    }

    void entity::physics(game *game, ship *ship, std::map<int, entity *> *missiles) {
        // check for collisions with ship or relevant missiles
    }
}