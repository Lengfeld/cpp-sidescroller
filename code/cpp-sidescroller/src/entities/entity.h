//
// Created by tobias on 07.03.21.
//

#pragma once

#include <map>
#include "../../extern/glm/glm/glm.hpp"
#include "../rendering/texture.h"

namespace spaceBattle {
    class game;
    class texture;
    class ship;

    class entity {
    public:
        entity(
                class game* game,
                texture* texture,
                int id,
                glm::vec2 position = glm::vec2(0.0f, 0.0f),
                glm::vec2 size = glm::vec2(50.0f, 50.0f),
                glm::vec2 velocity = glm::vec2(0.0f, 0.0f));

        virtual void update(game* game, float deltaTime);
        virtual void handleEvents(game* game, SDL_Event& e);
        virtual void draw(game *game);
        virtual void physics(game *game = nullptr, ship* ship = nullptr, std::map<int, entity*>* missiles = nullptr);

        glm::vec2 getPosition() { return m_position; }
        glm::vec2 getSize() { return m_size; }
        int getId() { return m_id; }

    protected:
        glm::vec2 m_position;
        glm::vec2 m_velocity;
        const glm::vec2 m_size;
        const int m_id;

        texture* m_texture;
    };
}