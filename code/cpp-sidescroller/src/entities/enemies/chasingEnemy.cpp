//
// Created by tobias on 13.03.21.
//

#include "chasingEnemy.h"
#include "../../game.h"


namespace spaceBattle {
    chasingEnemy::chasingEnemy(game *game, int id, texture *texture, ship *ship,
                               glm::vec2 position, glm::vec2 size, glm::vec2 velocity,
                               int destructionPrize, int armor, long fireRate) :
                               enemy(game, texture, id, position, size, velocity, ship, destructionPrize, armor),
                               m_fireRate(fireRate)
    {
        m_missileTimer.start();
    }

    void chasingEnemy::update(game *game, float deltaTime) {
        //Get window size
        int w = 0;
        int h = 0;
        SDL_GetWindowSize(game->getWindow(), &w, &h);

        // Move the ship left until it reaches 80% screenwidth
        if (m_position.x > 0.8 * w) { m_position.x += m_velocity.x * deltaTime; }

        // Navigate towards the height of the ship
        if (m_position.y < m_ship->getPosition().y) { m_position.y += m_velocity.y * deltaTime; }
        else if (m_position.y > m_ship->getPosition().y) { m_position.y -= m_velocity.y * deltaTime; }

        // If the ship went too far down
        if(m_position.y + m_size.y > h) { m_position.y -= m_velocity.y * deltaTime; }

        // Shoot missiles when timer reached interval
        if (m_missileTimer.getTicks() > m_fireRate)
        {
            game->spawnMissile(
                    hostile,
                    glm::vec2(m_position.x, m_position.y + 0.5 * m_size.y),
                    glm::vec2(600, 0));
            // restart timer
            m_missileTimer.start();
        }
    }
}