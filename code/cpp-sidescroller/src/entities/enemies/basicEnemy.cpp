//
// Created by tobias on 09.03.21.
//

#include "basicEnemy.h"
#include "../../game.h"

namespace spaceBattle {

    basicEnemy::basicEnemy(game *game, int id, texture *texture, ship *ship,
                           glm::vec2 position,
                           glm::vec2 size,
                           glm::vec2 velocity,
                           int destructionPrize, int armor) :
                           enemy(game, texture, id, position, size, velocity, ship, destructionPrize, armor)
    {
        //
    }

    void basicEnemy::update(game *game, float deltaTime) {
        //Get window size
        int w = 0;
        int h = 0;
        SDL_GetWindowSize(game->getWindow(), &w, &h);

        //Move the ship left
        m_position.x += m_velocity.x * deltaTime;

        //If the ship went too far to the left or right
        if(m_position.x < 0)
        {
            // Add to ship's missed enemy count
            m_ship->missedEnemy();
            game->deleteEnemy(m_id);
        }
    }
}