//
// Created by tobias on 13.03.21.
//

#include "enemy.h"
#include "../../game.h"

namespace spaceBattle {

    enemy::enemy(game *game, texture *texture, int id,
                 glm::vec2 position, glm::vec2 size, glm::vec2 velocity,
                 ship *ship, int destructionPrize, int armor) :
            entity(game, texture, id, position, size, velocity),
            m_ship(ship),
            m_destructionPrize(destructionPrize),
            m_armor(armor)
    {
        //
    }

    void enemy::physics(game *game, ship *ship, std::map<int, entity *> *missiles) {
        if (missiles != nullptr)
        {
            // check if colliding with ship
            if (checkCollision(this, ship))
            {
                game->deleteEnemy(getId());
                ship->addPoints(m_destructionPrize);
                ship->destroy(game);
            }

            // check missile collision
            for(auto &kvp : *missiles)
            {
                if (checkCollision(this, kvp.second))
                {
                    game->deleteMissile(friendly, kvp.second->getId());
                    if (--m_armor <= 0)
                    {
                        game->deleteEnemy(getId());
                        ship->addPoints(m_destructionPrize);
                    }
                }
            }
        }
    }

    bool enemy::checkCollision(entity* a, entity* b) {
        //The sides of the rectangles
        int leftA, leftB;
        int rightA, rightB;
        int topA, topB;
        int bottomA, bottomB;

        //Calculate the sides of the ship's rect
        leftA = a->getPosition().x;
        rightA = a->getPosition().x + a->getSize().x;
        topA = a->getPosition().y;
        bottomA = a->getPosition().y + a->getSize().y;

        //Calculate the sides of the missile's rect
        leftB = b->getPosition().x;
        rightB = b->getPosition().x + b->getSize().x;
        topB = b->getPosition().y;
        bottomB = b->getPosition().y + b->getSize().y;

        //If any of the sides from the ship are outside of the missile's rect
        if(bottomA <= topB) return false;
        if(topA >= bottomB) return false;
        if(rightA <= leftB) return false;
        if(leftA >= rightB) return false;

        return true;
    }
}