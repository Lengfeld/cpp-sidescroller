//
// Created by tobias on 13.03.21.
//

#pragma once

#include "enemy.h"
#include "../ship.h"

namespace spaceBattle {
    class chasingEnemy : public enemy {
    public:
        explicit chasingEnemy(game* game, int id, texture* texture,
                              ship* ship,
                              glm::vec2 position,
                              glm::vec2 size = glm::vec2(123.0f, 96.0f),
                              glm::vec2 velocity = glm::vec2(-150.0f, 100.0f),
                              int destructionPrize = 3, int armor = 3, long fireRate = 1000);

        void update(game *game, float deltaTime) override;

    private:
        timer       m_missileTimer;
        const long  m_fireRate;         // in milliseconds
    };
}


