//
// Created by tobias on 09.03.21.
//

#pragma once

#include "../ship.h"
#include "enemy.h"

namespace spaceBattle {
    class basicEnemy : public enemy {
    public:
        explicit basicEnemy(game* game, int id, texture* texture,
                            ship* ship,
                            glm::vec2 position,
                            glm::vec2 size = glm::vec2(120.0f, 64.0f),
                            glm::vec2 velocity = glm::vec2(-250.0f, 0.0f),
                            int destructionPrize = 1, int armor = 1);

        void update(game *game, float deltaTime) override;
    };
}


