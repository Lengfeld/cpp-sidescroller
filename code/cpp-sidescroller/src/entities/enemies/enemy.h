//
// Created by tobias on 13.03.21.
//

# pragma once

#include "../entity.h"

namespace spaceBattle {
    class enemy : public entity {
    public:
        enemy(game* game, texture* texture, int id,
              glm::vec2 position, glm::vec2 velocity, glm::vec2 size,
              ship* ship, int destructionPrize, int armor);

        void physics(game *game, ship *ship, std::map<int, entity *> *missiles) override;

    protected:
        bool checkCollision(entity* a, entity* b);

        ship*       m_ship = nullptr;
        const int   m_destructionPrize;
        int         m_armor;
    };
}

