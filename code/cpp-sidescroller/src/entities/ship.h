//
// Created by tobias on 07.03.21.
//

#pragma once

#include "entity.h"
#include "../game.h"
#include "../timer/timer.h"
#include "../rendering/animation.h"

namespace spaceBattle {
    class ship : public entity {
    public:
        explicit ship(game* game, texture* texture, int id, animation* destructAnim, glm::vec2 position,
                      glm::vec2 size = glm::vec2(50.0f, 50.0f), float speed = 550.0f, int lives = 3);

        void update(game* game, float deltaTime) override;
        void handleEvents(game* game, SDL_Event &e) override;
        void physics(game *game = nullptr, ship *ship = nullptr, std::map<int, entity *> *missiles = nullptr) override;
        void draw(game *game) override;

        void destroy(game* game);
        void addPoints(int amount);
        void missedEnemy();
        int getPoints() const;
        int getMissedEnemiesCount() const { return  m_missedEnemies; }
        int getMaxMissedEnemies() const { return m_maxMissedEnemies; }
        int getLives() const { return m_lives; }

    private:
        const float     m_speed;
        int             m_points = 0;
        int             m_missedEnemies = 0;
        const int       m_maxMissedEnemies = 5;
        int             m_lives = 3;

        timer           m_missileTimer;
        bool            m_shootMissiles = false;
        const long      m_fireRate = 350;         // in milliseconds

        bool            m_destroyed = false;
        animation*      m_destructAnim;
        float           m_curTimeStep = 0.0f;
    };
}

