//
// Created by tobias on 08.03.21.
//

#pragma once

#include <SDL2/SDL.h>

namespace spaceBattle {
    class timer {

    public:
        //Initializes variables
        timer();

        //The various clock actions
        void start();
        void stop();
        void pause();
        void unpause();

        //Gets the timer's time
        Uint32 getTicks() const;

        //Checks the status of the timer
        bool isStarted() const { return m_started; }
        bool isPaused() const { return m_started && m_paused; }

    private:
        //The clock time when the timer started
        Uint32 m_startTicks;

        //The ticks stored when the timer was paused
        Uint32 m_pausedTicks;

        //The timer status
        bool m_paused;
        bool m_started;
    };
}

