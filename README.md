# Cpp-sidescroller spacebattle

This is a 2D side scroller game, that has been developed as a coding challenge for an internship at Boxelware.

## Gameplay
The player controls a spaceship with 'WASD' and can shoot missiles by pressing 'Space'. 
There are two types of enemies that spawn on the right hand side of the game window: 
- **Basic enemies**: Those fly straight to the left hand side of the game window. If five or more reach it, the player loses one of their three lives. Basic enemies are destroyed after one hit. 
- **Chasing enemies**: Those try to destroy the player's ship by shooting missiles. Chasing enemies can take three hits before beeing destroyed themselves.

The enemies' spawnrates increase gradually with time passing.
If the player loses all of their lives, the game is over and they are asked to enter their name in the console.
After that the score is printed and added to a scoreboard containing the top ten entries, which can be printed to the console, including time and date.

## Technical details
The game runs on **Linux** (Ubuntu 18.04 or later), compiles with **g++**, uses **CMake** as a buildsystem and the **C++14** cpp-standard.
In addtition to that the project uses **SDL2** for rendering, input and window management and **glm** for vector math. 

## Requirements

### g++ compiler
To install g++ compiler: 

`sudo apt install build-essential`

### SDL2
To install SDL2: 

`sudo apt install libsdl2-dev`

`sudo apt install libsdl2-image-dev`

### CMake
Download CMake through the *Ubuntu Software* application. 

## Building
To build the project create a build folder in the code/cpp-sidescroller/ subdirectory and run the following:

`cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..`

`make all`

`./src/cpp_sidescroller`

## Credits
Many thanks to Matt Walkden for the amazing assets (https://mattwalkden.itch.io/lunar-battle-pack). 



